<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\ChatMessage
 *
 * @property int $id
 * @property int $from_id
 * @property int $to_id
 * @property string $message
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @property-read \App\Models\User $from
 * @property-read \App\Models\User $to
 * @property int $read
 * @method static \Database\Factories\ChatMessageFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessage myMessages()
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessage myReceivedMessages()
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessage mySentMessages()
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessage query()
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessage whereFromId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessage whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessage whereRead($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessage whereToId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChatMessage whereUpdatedAt($value)
 */
	class ChatMessage extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ChatRoom
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ChatMessage[] $messages
 * @property-read int|null $messages_count
 * @method static \Illuminate\Database\Eloquent\Builder|ChatRoom newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ChatRoom newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ChatRoom query()
 * @method static \Illuminate\Database\Eloquent\Builder|ChatRoom whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChatRoom whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChatRoom whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ChatRoom whereUpdatedAt($value)
 */
	class ChatRoom extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Swipe
 *
 * @property int $id
 * @property int $user_id
 * @property int $target_id
 * @property int $liked
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User|null $base
 * @property-read \App\Models\User $target
 * @method static \Illuminate\Database\Eloquent\Builder|Swipe liked()
 * @method static \Illuminate\Database\Eloquent\Builder|Swipe newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Swipe newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Swipe notLiked()
 * @method static \Illuminate\Database\Eloquent\Builder|Swipe notSwiped()
 * @method static \Illuminate\Database\Eloquent\Builder|Swipe query()
 * @method static \Illuminate\Database\Eloquent\Builder|Swipe whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Swipe whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Swipe whereLiked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Swipe whereTargetId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Swipe whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Swipe whereUserId($value)
 */
	class Swipe extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string|null $email_verified_at
 * @property string $password
 * @property string|null $gender
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Sanctum\PersonalAccessToken[] $personalAccessTokens
 * @property-read int|null $personal_access_tokens_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $unreadNotifications
 * @property-read int|null $unreadNotificationsCount
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Fortify\TwoFactorToken[] $twoFactorTokens
 * @property-read int|null $two_factor_tokens_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Jetstream\Profile[] $profiles
 * @property-read int|null $profiles_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Jetstream\Role[] $roles
 * @property-read int|null $roles_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Jetstream\Team[] $teams
 * @property-read int|null $teams_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Message[] $messages
 * @property-read int|null $messages_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Message[] $sentMessages
 * @property-read int|null $sent_messages_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Message[] $receivedMessages
 * @property-read int|null $received_messages_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Message[] $unreadMessages
 * @property-read int|null $unread_messages_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Message[] $readMessages
 * @property-read int|null $read_messages_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Message[] $deletedMessages
 * @property-read int|null $deleted_messages_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Message[] $archivedMessages
 * @property int|null $current_team_id
 * @property string|null $profile_photo_path
 * @property string|null $two_factor_secret
 * @property string|null $two_factor_recovery_codes
 * @property string|null $two_factor_confirmed_at
 * @property string|null $date_of_birth
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Swipe[] $dislikes
 * @property-read int|null $dislikes_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Swipe[] $dislikesOf
 * @property-read int|null $dislikes_of_count
 * @property-read string $profile_photo_url
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Swipe[] $likes
 * @property-read int|null $likes_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Swipe[] $likesOf
 * @property-read int|null $likes_of_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Swipe[] $swipes
 * @property-read int|null $swipes_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Sanctum\PersonalAccessToken[] $tokens
 * @property-read int|null $tokens_count
 * @method static \Database\Factories\UserFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCurrentTeamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDateOfBirth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereProfilePhotoPath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereTwoFactorConfirmedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereTwoFactorRecoveryCodes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereTwoFactorSecret($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

