<?php

namespace App\Http\Controllers;

use App\Events\NewChatRoomMessage;
use App\Models\ChatRoom;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    public function rooms(Request $request)
    {
        return ChatRoom::all();
    }

    public function messages(Request $request, ChatRoom $room)
    {
        $messages = $room->messages()->with('user')->get();
        return $messages;
    }

    public function sendMessage(Request $request, ChatRoom $room)
    {
        $message = $room->messages()->create([
            'user_id' => auth()->user()->id,
            'message' => $request->message,
        ]);

        broadcast(new NewChatRoomMessage($message))->toOthers();
        return $message;
        // return response()->json(['status' => 'ok']);
    }
}
