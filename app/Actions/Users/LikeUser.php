<?php

namespace App\Actions\Users;

use App\Models\User;
use App\Models\Swipe;
use Lorisleiva\Actions\Concerns\AsAction;

class LikeUser
{
    use AsAction;

    public function handle(User $user)
    {
        auth()->user()->swipes()->create([
            'target_id' => $user->id,
            'liked' => true,
        ]);

        if ($this->isMatch(auth()->user(), $user)) {
            $this->sendMatchNotification(auth()->user(), $user);
        }

        return response()->json(['message' => 'You liked ' . $user->name]);
    }

    public function isMatch(User $user, User $target): bool
    {
        return $user->likes()->where('target_id', $target->id)->where('liked', true)->exists()
            && $target->likes()->where('target_id', $user->id)->where('liked', true)->exists();
    }

    public function sendMatchNotification(User $user, User $target)
    {
        $user->notify(new \App\Notifications\MatchNotification($target));
        $target->notify(new \App\Notifications\MatchNotification($user));
    }
}
