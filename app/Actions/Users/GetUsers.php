<?php

namespace App\Actions\Users;

use App\Http\Resources\UserResource;
use App\Models\User;
use Inertia\Inertia;
use Lorisleiva\Actions\Concerns\AsAction;

class GetUsers
{
    use AsAction;

    protected $crossGender = [
        'male' => 'female',
        'female' => 'male',
    ];
    public function handle()
    {

        $gender = $this->crossGender[auth()->user()->gender];

        $users = $this->getUsers($gender);

        return Inertia::render('Dashboard', [
            'users' => UserResource::collection($users)
        ]);
    }

    public function getUsers(string $gender)
    {
        $swipedIds = auth()->user()->swipes()->pluck('target_id');
        $dislikedOfId = auth()->user()->dislikesOf()->pluck('user_id');

        $users = User::whereNotIn('id', $swipedIds->all())
            ->whereNotIn('user_id', $dislikedOfId->all())
            ->where('gender', $gender)
            ->paginate();
        return $users;
    }
}
